<?php

use App\Http\Controllers\API\UserController;
use App\Http\Controllers\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Auth routes
Route::prefix('auth')->group(function (){
    Route::post('register', [UserController::class, 'store']);
    Route::post('login', [UserController::class, 'login']);
});

// Protected routes
Route::middleware('auth:api')->group( function (){
    // Orders routes.
    Route::prefix('orders')->group(function (){
        Route::get('{orderId}', [OrderController::class, 'getOrderDetails']);
        Route::get('', [OrderController::class, 'getAllOrdersForUSer']);
        Route::post('', [OrderController::class, 'createNewOrder']);
        Route::put('{orderId}', [OrderController::class, 'updateOrderProducts']);
    });

});
