<h1>After configure your .env file please run those commands:</h1>

<div>
    <ul>
        <li>composer install</li>
        <li>php artisan key:generate</li>
        <li>php artisan migrate</li>
        <li>php artisan db:seed</li>
        <li>php artisan passport:install</li> 
</ul>
</div>
