<?php

namespace Database\Seeders;

use App\Enum\EPaymentType;
use App\Models\Payment;
use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Payment::query()->insert([
            ['type' => EPaymentType::CREDIT_CARD],
            ['type' => EPaymentType::WIRE_TRANSFER],
        ]);
    }
}
