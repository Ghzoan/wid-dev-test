<?php


namespace App\Managers;


use App\Repository\Eloquent\OrderRepository;
use App\Repository\Eloquent\PaymentRepository;
use Illuminate\Http\Response;

class OrderManager implements ManagerInterface
{
    /**
     * Order repository instance.
     *
     * @var OrderRepository
     */
    protected $_orderRepository;

    /**
     * Payment repository instance.
     *
     * @var PaymentRepository
     */
    protected $_paymentRepository;

    /**
     * Create a new instance from order manager
     *
     * @constructor
     *
     * @param OrderRepository $orderRepository
     * @param PaymentRepository $paymentRepository
     *
    */
    public function __construct(OrderRepository $orderRepository, PaymentRepository $paymentRepository)
    {
        $this->_orderRepository = $orderRepository;
        $this->_paymentRepository = $paymentRepository;
    }

    /**
     * Get order details.
     *
     * @param int $orderId
     *
     * @return SharedMessage
    */
    public function getOrderDetails(int $orderId): SharedMessage{
        try {
            // Get order from repo.
            $order = $this->_orderRepository->findById($orderId, ['*'], ['products', 'payment']);
            // Return results.
            return new SharedMessage('Get order details success', $order);
        }catch (\Exception $exception){
            return new SharedMessage($exception->getMessage(), [], 2, false, $exception, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get all orders for a user.
     *
     * @param int $userId
     *
     * @return SharedMessage
    */
    public function getAllOrdersForUser(int $userId): SharedMessage{
        try {
            // Get orders from repo.
            $orders = $this->_orderRepository->getAllOrdersForUser($userId, ['*'], ['products', 'payment']);
            // Return results.
            return new SharedMessage('Get orders success', $orders);
        }catch (\Exception $exception){
            return new SharedMessage($exception->getMessage(), [], 2, false, $exception, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a new order.
     *
     * @param array $orderAttributes
     *
     *
     * @return SharedMessage
    */
    public function createNewOrder(array $orderAttributes): SharedMessage{
        try {
            // Get payment by type.
            $payment = $this->_paymentRepository->getElementBy('type', $orderAttributes['payment_type']);
            // Add payment id to attributes.
            $orderAttributes['payment_id'] = $payment->id;
            // Create order.
            $order = $this->_orderRepository->create($orderAttributes);
            // Return results.
            return new SharedMessage('Create a new order success', $order);
        }catch (\Exception $exception){
            return new SharedMessage($exception->getMessage(), [], 2, false, $exception, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update order products.
     *
     * @param int $orderId
     * @param array $products
     *
     * @return SharedMessage
     *
    */
    public function updateOrderProducts(int $orderId, array $products): SharedMessage{
        try {
            // Find order.
            $order = $this->_orderRepository->findById($orderId);
            if (!empty($products)){
                // Attach products.
                $order->products()->attach($products);
            }
            // Return results.
            return new SharedMessage('Update order products success', $order->fresh(['products', 'payment']));
        }catch (\Exception $exception){
            return new SharedMessage($exception->getMessage(), [], 2, false, $exception, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
