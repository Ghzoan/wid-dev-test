<?php


namespace App\Managers;


use Illuminate\Http\Response;

class SharedMessage
{
    public $data;
    public $resultCode;
    public $status;
    public $message;
    public $exception;
    public $statusCode;

    public function __construct($message, $data = [], $resultCode = 1, $status = true, $exception = null, $statusCode = Response::HTTP_OK){
        $this->message = $message;
        $this->data = $data;
        $this->resultCode = $resultCode;
        $this->status = $status;
        $this->exception = $exception;
        $this->statusCode = $statusCode;
    }
}
