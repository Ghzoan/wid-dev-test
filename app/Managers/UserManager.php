<?php


namespace App\Managers;


use App\Repository\Eloquent\UserRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserManager implements ManagerInterface
{
    /**
     * User repository instance.
     *
     * @var UserRepository
     */
    protected $_userRepository;

    /**
     * Create a new instance form user manager.
     *
     * @constructor
     *
     * @param UserRepository $userRepository
     *
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->_userRepository = $userRepository;
    }

    /**
     * Create a new user.
     *
     * @param array $userAttributes
     *
     * @return SharedMessage
    */
    public function createNewUser(array $userAttributes): SharedMessage{
        try {
            // password hashing.
            $userAttributes['password'] = Hash::make($userAttributes['password']);
            // Create a new record in db.
            $user = $this->_userRepository->create($userAttributes);
            // Create token of this user.
            $user->token = $user->createToken('UserToken')->accessToken;
            // Return Results.
            return new SharedMessage('Create a new user success', $user);
        }catch (\Exception $exception){
            return new SharedMessage($exception->getMessage(), [], 2, false, $exception, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Login user.
     *
     * @param string $email
     * @param string $password
     *
     * @return SharedMessage
     *
    */
    public function login(string $email, string $password): SharedMessage{
        try {
            // Find user by mail.
            $user = $this->_userRepository->getElementBy('email', $email);
            // Check on user password.
            if (!Hash::check($password, $user->password)){
                return new SharedMessage('Not valid inputs', [], 2, false);
            }
            // Login user
            Auth::login($user);
            // Generate access token for this user.
            $user->token = $user->createToken('UserToken')->accessToken;
            // Return results.
            return new SharedMessage('Login success', $user);
        }catch (\Exception $exception){
            return new SharedMessage($exception->getMessage(), [], 2, false, $exception, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
