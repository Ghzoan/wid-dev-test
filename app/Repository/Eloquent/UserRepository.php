<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * Create a new instance form user repository.
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
}
