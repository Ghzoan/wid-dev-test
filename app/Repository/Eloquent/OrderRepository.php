<?php


namespace App\Repository\Eloquent;


use App\Models\Order;
use App\Repository\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class OrderRepository extends BaseRepository implements OrderRepositoryInterface
{
    /**
     * Create a new instance form order repository.
     * @param Order $model
     */
    public function __construct(Order $model)
    {
        parent::__construct($model);
    }

    /**
     * Create a model.
     *
     * @param array $payload
     * @return Model
     */
    public function create(array $payload): ?Model
    {
        // Create order on db.
        $model = $this->model->create($payload);
        // Attach products.
        $model->products()->attach($payload['products']);
        // Return results.
        return $model->fresh();
    }

    /**
     * Get all orders for user.
     *
     * @param int $userId
     * @param array $columns
     * @param array $relations
     *
     * @return Collection
     */
    public function getAllOrdersForUser(int $userId, array $columns = ['*'], array $relations = []): Collection
    {
        return $this->model
            ->with($relations)
            ->where('user_id', '=', $userId)
            ->get($columns);
    }
}
