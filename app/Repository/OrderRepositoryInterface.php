<?php


namespace App\Repository;


use Illuminate\Database\Eloquent\Collection;

interface OrderRepositoryInterface
{
    /**
     * Get all orders for user.
     *
     * @param int $userId
     * @param array $columns
     * @param array $relations
     *
     * @return Collection
    */
    public function getAllOrdersForUser(int $userId, array $columns = ['*'], array $relations = []): Collection;
}
