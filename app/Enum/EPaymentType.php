<?php


namespace App\Enum;


class EPaymentType
{
    public const CREDIT_CARD = 'credit_card';
    public const WIRE_TRANSFER = 'wire_transfer';
}
