<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\LoginRequest;
use App\Managers\UserManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Repository\Eloquent\UserRepository;

class UserController extends Controller
{
    /**
     * User repository instance.
     *
     * @var UserManager
     */
    protected $_userManager;

    /**
     * Create a new instance form user controller.
     *
     * @constructor
     *
     * @param UserManager $userManager
     *
    */
    public function __construct(UserManager $userManager)
    {
        $this->_userManager = $userManager;
    }

    /**
     * Create a new user.
     *
     * @param CreateUserRequest $request
     *
     * @return JsonResponse
    */
    public function store(CreateUserRequest $request): JsonResponse{
        // Get attributes from request.
        $attributes = $request->only(['name', 'surname', 'email', 'password']);
        // Get message from manager.
        $createUserMessage = $this->_userManager->createNewUser($attributes);
        // Return results.
        return $this->handleShredMessage($createUserMessage);
    }

    /**
     * Login user.
     *
     * @param LoginRequest $request
     *
     * @return JsonResponse
    */
    public function login(LoginRequest $request): JsonResponse{
        // Get login message from manager.
        $loginMessage = $this->_userManager->login($request->get('email'), $request->get('password'));
        // Return results.
        return $this->handleShredMessage($loginMessage);
    }
}
