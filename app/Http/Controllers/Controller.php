<?php

namespace App\Http\Controllers;


use App\Managers\SharedMessage;
use App\Traits\APIResponse;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, APIResponse;

    /**
     * Handle manager messages.
     * @param SharedMessage $message
     * @return JsonResponse
     */
    protected function handleShredMessage(SharedMessage $message): JsonResponse
    {
        // Check on message status.
        if ($message->status){
            // Return success response.
            return $this->success(
                $message->message,
                $message->data,
                $message->resultCode,
                $message->statusCode ?? JsonResponse::HTTP_OK
            );
        }
        // Handle error of this message.
        return $this->error(
            $message->message,
            $message->resultCode,
            $message->statusCode ?? JsonResponse::HTTP_BAD_REQUEST
        );
    }
}
