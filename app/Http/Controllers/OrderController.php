<?php

namespace App\Http\Controllers;


use App\Http\Requests\Order\CreateOrderRequest;
use App\Http\Requests\Order\GetUserOrdersRequest;
use App\Http\Requests\Order\UpdateOrderProductsRequest;
use App\Managers\OrderManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Order manager instance.
     *
     * @var OrderManager
     */
    protected $_orderManager;

    /**
     * Create a new instance from order controller.
     *
     * @constructor
     *
     * @param OrderManager $orderManager
    */
    public function __construct(OrderManager $orderManager)
    {
        $this->_orderManager = $orderManager;
    }

    /**
     * Get order details.
     *
     * @param int $orderId
     *
     * @return JsonResponse
    */
    public function getOrderDetails(int $orderId): JsonResponse{
        // Get message from manager.
        $getOrderDetailsMessage = $this->_orderManager->getOrderDetails($orderId);
        // Return results.
        return $this->handleShredMessage($getOrderDetailsMessage);
    }

    /**
     * Get all orders for a user.
     *
     * @param GetUserOrdersRequest $request
     *
     * @return JsonResponse
    */
    public function getAllOrdersForUSer(GetUserOrdersRequest $request): JsonResponse{
        // Get message from manager.
        $getUserOrdersMessage = $this->_orderManager->getAllOrdersForUser($request->get('user_id'));
        // Return results.
        return $this->handleShredMessage($getUserOrdersMessage);
    }

    /**
     * Create a new order.
     *
     * @param CreateOrderRequest $request
     *
     * @return JsonResponse
    */
    public function createNewOrder(CreateOrderRequest $request): JsonResponse{
        // Get attributes from request.
        $attributes = $request->only(['user_id', 'products', 'payment_type', 'price']);
        // Get message from manager.
        $createOrderMessage = $this->_orderManager->createNewOrder($attributes);
        // Return results.
        return $this->handleShredMessage($createOrderMessage);
    }

    /**
     * Update order products.
     *
     * @param UpdateOrderProductsRequest $request
     * @param int $orderId
     *
     * @return JsonResponse
    */
    public function updateOrderProducts(UpdateOrderProductsRequest $request, int $orderId): JsonResponse{
        // Get message from manager.
        $updateOrderProductsMessage = $this->_orderManager->updateOrderProducts(
            $orderId,
            $request->get('product_id') ? [$request->get('product_id') ] : []
        );
        // Return results.
        return $this->handleShredMessage($updateOrderProductsMessage);
    }
}
