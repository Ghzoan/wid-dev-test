<?php

namespace App\Http\Requests;


use App\Traits\APIResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class MainRequest extends FormRequest
{
    use APIResponse;

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @return void
     *
     * @throws ValidationException
     */
    public function failedValidation(Validator $validator): void
    {
        // Check on accept key in the header of the request.
        if (request()->header('accept') !== 'application/json'){
            // Call on parent logic.
            parent::failedValidation($validator);
        }else{
            // Get errors from validator.
            $errors = implode(",",$validator->messages()->all());
            // Throw exceptions.
            throw new HttpResponseException(
                $this->error((string)$errors, 0, Response::HTTP_BAD_REQUEST)
            );
        }
    }
}
