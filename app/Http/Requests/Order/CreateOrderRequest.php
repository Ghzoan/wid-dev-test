<?php

namespace App\Http\Requests\Order;

use App\Enum\EPaymentType;
use App\Http\Requests\MainRequest;
use Illuminate\Validation\Rule;

class CreateOrderRequest extends MainRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|integer|exists:users,id',
            'products' => 'required|array',
            'payment_type' => ['required', Rule::in([EPaymentType::CREDIT_CARD, EPaymentType::WIRE_TRANSFER])],
            'price' => 'required|numeric'
        ];
    }
}
