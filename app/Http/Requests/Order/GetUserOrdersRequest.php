<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\MainRequest;
use Illuminate\Foundation\Http\FormRequest;

class GetUserOrdersRequest extends MainRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required'
        ];
    }
}
