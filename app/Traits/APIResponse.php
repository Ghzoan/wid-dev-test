<?php
namespace App\Traits;
use Illuminate\Http\JsonResponse;

trait APIResponse
{
    /**
     * Core of response
     *
     * @param string $message
     * @param mixed $data
     * @param int $resultCode
     * @param integer $statusCode
     * @param boolean $isSuccess
     * @return JsonResponse
     */
    public function coreResponse(string $message, $data, int $resultCode,int $statusCode, bool $isSuccess = true): JsonResponse
    {
        // Check the params
        if(!$message) return response()->json(['message' => 'Message is required'], 500);

        // Send the response
        if($isSuccess) {
            return response()->json([
                'message' => $message,
                'status' => true,
                'data' => $data,
                'result_code' => $resultCode
            ], $statusCode);
        } else {
            return response()->json([
                'message' => $message,
                'status' => false,
                'data' => null,
                'result_code' => $resultCode
            ], $statusCode);
        }
    }

    /**
     * Send any success response
     *
     * @param string $message
     * @param array|object $data
     * @param int $resultCode
     * @param integer $statusCode
     * @return JsonResponse
     */
    public function success(string $message, $data, int $resultCode = 1 ,int $statusCode = 200): JsonResponse
    {
        return $this->coreResponse($message, $data, $resultCode, $statusCode);
    }

    /**
     * Send any error response
     *
     * @param string $message
     * @param int $resultCode
     * @param integer $statusCode
     * @return JsonResponse
     */
    public function error(string $message, int $resultCode = 2, int $statusCode = 500): JsonResponse
    {
        return $this->coreResponse($message, null, $resultCode, $statusCode, false);
    }
}
