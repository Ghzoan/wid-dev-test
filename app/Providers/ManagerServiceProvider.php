<?php

namespace App\Providers;

use App\Managers\ManagerInterface;
use App\Managers\OrderManager;
use App\Managers\UserManager;
use Illuminate\Support\ServiceProvider;

class ManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ManagerInterface::class, UserManager::class);
        $this->app->bind(ManagerInterface::class, OrderManager::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
